## Setup

In order to be able to pull from the remote host, an authentication with the GitLab server needs
to be set up. The easiest way to do this is by using a public key method. So on the external server
the public key of the GitLab server user, that runs the jobs has to be added to the remote server.
The file to store that key is usually located under `~/.ssh/authorized_keys`. A public
key looks as followed:

```
ssh-rsa AAAAB3N.... username
```

After adding this public key and saving the file, a deployment should from GitLab should be possible.
If the file didn't existed and was created, the file permissions have to be set as followed:

```
chmod 400 ~/.ssh/authorized_keys
```

## Example (staging only, with auto-deployment of any branch)

```yml
include:
  - 'https://gitlab.com/2ndkauboy/gitlab-ci/raw/master/templates/.ssh-git-deployment.yml'
  - 'https://gitlab.com/2ndkauboy/gitlab-ci/raw/master/templates/deploy-ssh-staging.yml'

variables:
  SSH_HOST: www.example.com@www.example.com
  SSH_DOCUMENT_ROOT: /path/to/document/root/
```

## Example (staging only, with auto-deployment only from master branch)

```yml
include:
  - 'https://gitlab.com/2ndkauboy/gitlab-ci/raw/master/templates/.ssh-git-deployment.yml'
  - 'https://gitlab.com/2ndkauboy/gitlab-ci/raw/master/templates/deploy-ssh-staging.yml'

deploy:staging:
  variables:
    SSH_HOST: www.example.com@www.example.com
    SSH_DOCUMENT_ROOT: /path/to/document/root/
  only:
    - master

```

## Example (staging, with manual deployment from any branch and production, with manual deployment from tags only)
```yml
include:
  - 'https://gitlab.com/2ndkauboy/gitlab-ci/raw/master/templates/.ssh-git-deployment.yml'
  - 'https://gitlab.com/2ndkauboy/gitlab-ci/raw/master/templates/deploy-ssh-staging.yml'
  - 'https://gitlab.com/2ndkauboy/gitlab-ci/raw/master/templates/deploy-ssh-production.yml'

variables:
  SSH_HOST: www.example.com@www.example.com
  SSH_DOCUMENT_ROOT: /path/to/document/root/

deploy:staging:
  environment:
    url: https://staging.example.com
  variables:
    SSH_DOCUMENT_ROOT: ~/domains/staging.example.com/public_html
  when: manual

deploy:production:
  environment:
    url: https://www.example.com
  only:
  - tags
  when: manual
```