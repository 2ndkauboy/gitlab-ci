## Setup

The repository needs a `phpcs.xml` file. An example of such a file can be found in this repository. 
To activate the codings standards checks there are only needed to lines in the `gitlab-ci.yml` file:

## Example (checking coding standards only)

```yml
include:
  - 'https://gitlab.com/2ndkauboy/gitlab-ci/raw/master/templates/test-phpcs-wordpress.yml'
```
